Added:
- Select text for media description

changed:
- Increase media description length to 1500 chars
- More details about accounts in notifications
- Media management in timelines
- Allow cross-account replies on followed instances

Fixed:
- Remove extra spaces at the bottom of messages
- Some issue with custom emoji
- Fix issue with "Your toots/notifications"
- Fix issue with CW and Pixelfed
- Fix some media not loaded in profiles
- Scheduled toots from server side have an incorrect date
- Incorrect feeds when checking instance admin account
- Some crashes