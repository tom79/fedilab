Added:
- Automatically add hashtag to messages when composing from a search

Fixed:
- Some issues with content and URLs
- Some crashes