Fixed:
- An issue with animated avatars in menu
- An issue with avatars in accounts list
- An issue with banana.dog instance
- An issue with sensitive media when there were already a video preview
- Crashes with polls on some instances
- Crashes with Pixelfed
- Some other issues.