/* Copyright 2019 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */
package app.fedilab.android.asynctasks;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import java.lang.ref.WeakReference;

import app.fedilab.android.client.APIResponse;
import app.fedilab.android.client.PixelfedAPI;
import app.fedilab.android.interfaces.OnRetrieveStoriesInterface;


/**
 * Created by Thomas on 02/11/2019.
 * Retrieves stories on the instance
 */

public class RetrieveStoriesAsyncTask {


    private final String max_id;
    private final OnRetrieveStoriesInterface listener;
    private final WeakReference<Context> contextReference;
    private APIResponse apiResponse;
    private final String userId;

    public RetrieveStoriesAsyncTask(Context context, String max_id, String userId, OnRetrieveStoriesInterface onRetrieveStoriesInterface) {
        this.contextReference = new WeakReference<>(context);
        this.max_id = max_id;
        this.listener = onRetrieveStoriesInterface;
        this.userId = userId;
        doInBackground();
    }

    protected void doInBackground() {
        new Thread(() -> {
            PixelfedAPI pixelfedAPI = new PixelfedAPI(this.contextReference.get());
            apiResponse = pixelfedAPI.getFriendStories(userId, max_id);
            Handler mainHandler = new Handler(Looper.getMainLooper());
            Runnable myRunnable = () -> listener.onRetrieveStories(apiResponse);
            mainHandler.post(myRunnable);
        }).start();
    }


}
